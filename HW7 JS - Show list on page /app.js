// // // DOM - это объектная  модель документа с помощью какой, мы можем через JS обращаться к DOM элементам и "оживлять" страницу

const words = [
  "lights",
  "Dima",
  ["Lubny", "Poltava"],
  23,
  "data-science",
  true,
  "function-23",
];
// console.log(Array.isArray(words));
// console.log(words.constructor === Array);
const res = [];
const ulItems = document.querySelector(".itemList");
console.log(ulItems);
function getItems(words, ulItems) {
  const mapItem = words.map(function (item) {
    return `<li>${item}</li>`;
  });
  ulItems.insertAdjacentHTML("afterbegin", mapItem.join(""));
  // Проверяем на вложеность массива, не получилось(
  words.forEach(function (item) {
    if (Array.isArray(item)) {
      res.push(item);
      const attUl = document.createElement("ul");
      res.forEach(function (item) {
        const attLi = document.createElement("li");
        attLi.innerHTML = `${item.join("")}`;
        attUl.insertAdjacentHTML("afterbegin", attLi);
      });
    }
  });
}
console.log(getItems(words, ulItems));

// Это я пытался решить немного другим способом, что бы вложенный список отображался на стр

// const words = [
//   "lights",
//   "Dima",
//   ["Borispol", "Irpin"],
//   23,
//   "data-science",
//   true,
//   "function-23",
// ];
// const res = [];
// const items = document.querySelector(".itemList");
// function getItems(words, items) {
//   words.forEach(function (item) {
//     if (Array.isArray(item)) {
//       res.push(item);
//       const attUl = document.createElement("ul");
//       res.forEach(function (item) {
//         console.log(item);
//         console.log(res.length);
//         const attLiTag = document.createElement("li");
//         attLiTag.innerHTML = item;
//         attUl.appendChild(attLiTag);
//       });

//       items.appendChild(attUl);
//     } else {
//       let liTag = document.createElement("li");
//       liTag.innerHTML = item;
//       items.appendChild(liTag);
//     }
//   });
// }
// console.log(res);
// console.log(getItems(words, items));

const out = {
  add: 5,
  dsg: "Skoda",
  true: "false",
};
// console.log(Object.keys(out));
// console.log(out.hasOwnProperty("dsg"));
console.log("add" in out);
