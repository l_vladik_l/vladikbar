const centeredContent = document.querySelector(".centered-content");
const tabsLi = document.querySelectorAll(".tabs li");
const tabsContentLi = document.querySelectorAll(".tabs-content li");
let selectedLi;

centeredContent.addEventListener("click", function change(e) {
  let target = e.target;

  if (target.className === "tabs-title") {
    target.classList.add("active");

    if (selectedLi) {
      selectedLi.classList.remove("active");
    }
    selectedLi = target;
    selectedLi.classList.add("active");

    for (let i = 0; i < tabsLi.length; i++) {
      if (target === tabsLi[i]) {
        showText(i);
      }
    }
  }
});

function showText(i) {
  if (tabsContentLi[i].classList.contains("hidden")) {
    clearText();
    tabsContentLi[i].classList.remove("hidden");
    tabsContentLi[i].classList.add("active");
  }
}

function clearText() {
  for (let i = 0; i < tabsContentLi.length; i++) {
    tabsContentLi[i].classList.remove("active");
    tabsContentLi[i].classList.add("hidden");
  }
}
