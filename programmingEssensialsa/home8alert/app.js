// var это переменная, которая выносится в отдельный "список", перед тем как код будет выполняться, так же var можно переназначать.
// let это переменная, которая можно изменять во время выполнение кода, const это переменная, которая не может быть перезаписаная, будет ошибка.
// Использовать переменную var считается плохой практикой, так как мы должны использовать те переменные, которые мы обьявили ранее.

// ----------------------------------------------------------------

let askNumber = +prompt("Tell me please number ", "18");
let greeting = alert(`
.∧＿∧
( ･ω･｡)つ━☆・*。
⊂  ノ    ・゜+.
しーＪ   °。+ *´¨)
         .· ´¸.·*´¨)
          (¸.·´ (¸.·'* hello Saribeg☆`);
let isBoss = confirm("is boss?");
alert(isBoss);

// ----------------------------------------------------------------

let name = prompt("Tell me please u name?");
let age = +prompt("Tell me please u age?");
let maxAge = 22;

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age <= maxAge) {
  let conf = confirm("Are you sure you want to continue?");
  let checkConfirm = conf
    ? alert(`Welcome ${name}`)
    : !conf
    ? alert("You are not allowed to visit this website")
    : "";
} else {
  alert(`Welcome ${name}`);
}
