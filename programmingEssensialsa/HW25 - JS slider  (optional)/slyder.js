const previous = document.querySelector(".previous");
const next = document.querySelector(".next");
const wrapperGallery = document.querySelector(".wrapper-gallery");
let clickerNext = 0;

next.addEventListener("click", function (e) {
  if (clickerNext === wrapperGallery.children.length - 1) {
    wrapperGallery.children[wrapperGallery.children.length - 1].classList.add(
      "hide"
    );
    clickerNext = 0;
    wrapperGallery.children[clickerNext].classList.remove("hide");
  } else {
    wrapperGallery.children[clickerNext].classList.add("hide");
    wrapperGallery.children[clickerNext + 1].classList.remove("hide");

    clickerNext++;
  }
});

let clickerThree = wrapperGallery.children.length;
let clickerOne = 1;
let click2 = 1;

previous.addEventListener("click", function (e) {
  if (clickerOne === 4 || click2 === 4) {
    clickerThree = wrapperGallery.children.length;
    clickerOne = 1;
    click2 = 1;
  }
  wrapperGallery.children[
    wrapperGallery.children.length - clickerThree
  ].classList.add("hide");
  clickerThree = clickerOne;
  clickerOne++;
  console.log(clickerOne);

  wrapperGallery.children[
    wrapperGallery.children.length - click2
  ].classList.remove("hide");
  click2++;
  console.log(click2);
});
