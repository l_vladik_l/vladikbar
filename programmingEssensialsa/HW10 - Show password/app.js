const showHidePassword = document.querySelector(".inputToggle");
const inputToggleConfirm = document.querySelector(".inputToggleConfirm");
const eyeShow = document.querySelector(".eyeShow");
const btn = document.querySelector(".btn");
const eyeHide = document.querySelector(".eyeHide");
const label = document.querySelector(".input-wrapper");

eyeShow.addEventListener("click", function (e) {
  const type = showHidePassword.getAttribute("type");
  showHidePassword.setAttribute(
    "type",
    type === "password" ? "text" : "password"
  );
  eyeShow.style.display = "none";
  eyeHide.style.display = "block";
});

eyeHide.addEventListener("click", function (e) {
  const type = showHidePassword.getAttribute("type");
  showHidePassword.setAttribute(
    "type",
    type === "password" ? "text" : "password"
  );
  eyeHide.style.display = "none";
  eyeShow.style.display = "block";
});
btn.addEventListener("click", function (e) {
  e.preventDefault();
  if (showHidePassword.value === inputToggleConfirm.value) {
    alert("You are welcome");
  } else {
    let warn = "Нужно ввести одинаковые значения";
    inputToggleConfirm.insertAdjacentHTML(
      "afterend",
      `<div class="warnDiv"><br />${warn}</div>`
    );
    const warnDiv = document.querySelector(".warnDiv");
    function deleteWarnDiv() {
      warnDiv.remove();
    }
    setTimeout(deleteWarnDiv, 3000);
    warnDiv.addEventListener("click", (e) => console.log(e));
  }
});
