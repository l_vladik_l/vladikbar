const div = document.querySelector(".div");
const input = document.querySelector("#inputPrice");

input.addEventListener("focus", function (e) {
  e.target.style.border = "3px solid green";
});
input.addEventListener("blur", function (e) {
  if (input.value < 0) {
    input.style.border = "3px solid red";
    let warn = "Please enter correct price";
    input.insertAdjacentHTML(
      "afterend",
      `<div class="warnDiv"><br />${warn}</div>`
    );
    const warnDiv = document.querySelector(".warnDiv");
    function deleteWarnDiv() {
      if (document.body.contains(warnDiv)) {
        div.removeChild(warnDiv);
      }
    }
    setTimeout(deleteWarnDiv, 3000);
  } else {
    e.target.style.border = "";
    input.insertAdjacentHTML(
      "afterend",
      `<span class="inSpan">Текущая цена : ${input.value}<button class="close">X</button> </span>`
    );
    const inSpan = document.querySelector(".inSpan");
    const inText = document.querySelector(".inSpan");
    inText.style.color = "green";
    const btn = document.querySelector(".close");
    btn.addEventListener("click", function () {
      div.removeChild(inSpan);
      input.value = "";
    });
  }
});
